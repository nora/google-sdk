<?php

declare(strict_types=1);

namespace NORA\GoogleSdk;

use Google\Client;
use NORA\GoogleSdk\VO\AccessToken;

interface GoogleSdkInterface
{
    public function authenticated(AccessToken $token): Client;
    public function getClient(): Client;
}
