<?php

namespace NORA\GoogleSdk;

use NORA\GoogleSdk\VO\GoogleSdkConfig;
use Ray\Di\Di\Named;
use Ray\Di\ProviderInterface;
use Ray\Di\SetContextInterface;

/**
 * @template-implements ProviderInterface<GoogleSdkInterface>
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class GoogleSdkProvider implements ProviderInterface, SetContextInterface
{
    private string $context;

    public function __construct(
        /** @var array<GoogleSdkConfig> */
        #[Named('google_sdk_configs')] private array $clientConfig
    ) {
    }

    public function setContext($context): void
    {
        $this->context = $context;
    }

    public function get(): GoogleSdkInterface
    {
        $context = $this->context;

        if (!isset($this->clientConfig[$context])) {
            $context = 'default';
            if (!isset($this->clientConfig[$context])) {
                throw new \RuntimeException('Configuration not found');
            } else {
            }
        }

        assert($this->clientConfig[$context] instanceof GoogleSdkConfig);
        return new GoogleSdk($this->clientConfig[$context]);
    }
}
