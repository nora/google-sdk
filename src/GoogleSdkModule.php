<?php

namespace NORA\GoogleSdk;

use NORA\GoogleSdk\Usecase\CalendarRead;
use NORA\GoogleSdk\Usecase\CreateOAuthToken;
use NORA\GoogleSdk\Usecase\SpreadSheetRead;
use Ray\Di\AbstractModule;

final class GoogleSdkModule extends AbstractModule
{
    public function __construct(
        /** @var array<\NORA\GoogleSdk\VO\GoogleSdkConfig> */
        private array $config = []
    ) {
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->bind()
            ->annotatedWith('google_sdk_configs')
            ->toInstance($this->config);
        $this->bind(GoogleSdkInterface::class)->toProvider(GoogleSdkProvider::class);
        foreach (array_keys($this->config) as $k) {
            $this->bind(GoogleSdkInterface::class)
                ->annotatedWith($k)
                ->toProvider(GoogleSdkProvider::class, $k);
        }
        $this->bind(CreateOAuthToken::class);
        $this->bind(SpreadSheetRead::class);
        $this->bind(CalendarRead::class);
    }
}
