<?php

declare(strict_types=1);

namespace NORA\GoogleSdk;

use Google\Client as GoogleClient;
use NORA\GoogleSdk\VO\AccessToken;
use NORA\GoogleSdk\VO\GoogleSdkConfig;
use NORA\Oauth\AccessTokenInterface;
use NORA\Oauth\AccessTokenRefreshInterface;

final class GoogleSdk implements GoogleSdkInterface, AccessTokenRefreshInterface
{
    public function __construct(
        private GoogleSdkConfig $config,
    ) {
    }

    public function getClient(): GoogleClient
    {
        return $this->config->configureClient(new GoogleClient());
    }

    public function authenticated(AccessTokenInterface $token): GoogleClient
    {
        assert($token instanceof AccessToken);
        $client = $this->getClient();
        return $token->setAccessToken($client);
    }

    public function refresh(AccessTokenInterface $token, ?bool &$refreshed = null): AccessTokenInterface
    {
        assert($token instanceof AccessToken);
        $client = $this->authenticated($token);
        if (!$client->isAccessTokenExpired()) {
            $refreshed = false;
            return $token;
        }
        $refreshed = true;
        return $token->refresh($this->authenticated($token));
    }

    public function forceRefresh(AccessTokenInterface $token): AccessTokenInterface
    {
        assert($token instanceof AccessToken);
        return $token->refresh($this->authenticated($token));
    }

    public function isAccessTokenExpired(AccessTokenInterface $token): bool
    {
        $client = $this->authenticated($token);
        return $client->isAccessTokenExpired();
    }
}
