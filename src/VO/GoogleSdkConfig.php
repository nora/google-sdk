<?php

declare(strict_types=1);

namespace NORA\GoogleSdk\VO;

use Google\Client;

final class GoogleSdkConfig
{
    public function __construct(
        private ?string $credentialsJson = null,
        private string $name = "NORA GoogleSdk",
        /** @var array<string> */
        private array $scopes = ['email']
    ) {
    }

    public function configureClient(Client $client): Client
    {
        $client->setApplicationName($this->name);
        $client->setScopes($this->scopes);
        if (is_string($this->credentialsJson)) {
            $client->setAuthConfig($this->credentialsJson);
        }
        return $client;
    }
}
