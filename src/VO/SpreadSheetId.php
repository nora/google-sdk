<?php

namespace NORA\GoogleSdk\VO;

final class SpreadSheetId
{
    public function __construct(?string $url = null, private ?string $id = null)
    {
        if (is_string($url) && preg_match('/^https:/', $url)) {
            $parts = parse_url($url);
            assert(isset($parts['path']));
            $this->id = basename(dirname($parts['path']));
        }
    }

    public function __toString(): string
    {
        return (string) $this->id;
    }
}
