<?php

declare(strict_types=1);

namespace NORA\GoogleSdk\VO;

use Google\Client;
use NORA\Oauth\AccessTokenInterface;
use RuntimeException;

use function PHPUnit\Framework\assertTrue;

final class AccessToken implements AccessTokenInterface
{
    public function __construct(private string $json)
    {
    }

    public function setAccessToken(Client $client, bool $refresh = true): Client
    {
        $client->setAccessToken((string) $this);
        if ($refresh && $client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
        return $client;
    }

    public function refresh(Client $client): AccessToken
    {
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
        return AccessToken::fromClient($client);
    }

    public function __toString(): string
    {
        return $this->json;
    }

    public static function fromFile(string $path): self
    {
        assertTrue(is_readable($path));
        $token = file_get_contents($path);
        if (!is_string($token)) {
            throw new RuntimeException("File dose not hava an AccessToken");
        }
        return self::fromString($token);
    }

    public static function fromString(string $token): self
    {
        return new self(json: $token);
    }

    public static function fromClient(Client $client): self
    {
        $token = json_encode($client->getAccessToken());
        if (!is_string($token)) {
            throw new RuntimeException("Client dose not hava an AccessToken");
        }
        return self::fromString($token);
    }

    public function getToken(): string
    {
        return json_decode($this->json)->access_token;
    }

    public function hasExpired(): bool
    {
        $token = json_decode($this->json);
        $expire = $token->created + $token->expires_in;
        if (time() > $expire) {
            return false;
        }
        return true;
    }

    public function getRefreshToken(): string
    {
        return json_decode($this->json)->refresh_token;
    }
}
