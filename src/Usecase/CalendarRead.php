<?php

namespace NORA\GoogleSdk\Usecase;

use DateTime;
use Generator;
use NORA\GoogleSdk\GoogleSdkInterface;
use Google\Service\Calendar as GoogleCalendar;
use DateTimeInterface;

/**
 * カレンダーから予定を取得する
 */
final class CalendarRead
{
    /**
     * @var array<string>
     * @psalm-suppress PossiblyUnusedProperty
     */
    public static array $scopes = [
        GoogleCalendar::CALENDAR_READONLY
    ];
    /** @var array<string> */
    public static array $wdaynames = [
        '日',
        '月',
        '火',
        '水',
        '木',
        '金',
        '土',
    ];

    public function __construct(
        private GoogleSdkInterface $sdk,
    ) {
    }

    /**
     * @psalm-return Generator
     */
    public function __invoke(
        string $calendar_id,
        DateTimeInterface $fromDateTime,
        DateTimeInterface $toDateTime,
        int $maxResults = 100,
        string $orderBy = 'startTime',
        string $timezone = 'Asia/Tokyo'
    ): Generator {
        $client = $this->sdk->getClient();
        $calendar = new GoogleCalendar($client);
        /** @psalm-suppress ArgumentTypeCoercion */
        date_default_timezone_set($timezone);

        $result = $calendar->events->listEvents(
            $calendar_id,
            [
                'maxResults' => $maxResults,
                'orderBy' => $orderBy,
                'singleEvents' => true,
                'timeMin' => $fromDateTime->format('c'),
                'timeMax' => $toDateTime->format('c'),
            ]
        );

        foreach ($result->getItems() as $item) {
            $data = [
                'author_name' => strtok($item->creator->email, '@'),
                'author_icon' => sprintf(
                    'https://www.gravatar.com/avatar/%s?s=80&d=mm&r=g',
                    md5(strtolower(trim((string) $item->creator->email)))
                ),
                'summary' => $item->getSummary(),
                'description' => $item->getDescription(),
                'htmlLink' => $item->getHtmlLink()
            ];

            if ($item->start->getDateTime()) {
                $data['youbi'] = static::$wdaynames[intval((new DateTime($item->start->getDateTime()))->format('w'))];
                $data['start'] = (new DateTime($item->start->getDateTime()))->format('y/m/d H:i');
            }
            if ($item->end->getDateTime()) {
                $data['end'] = (new DateTime($item->end->getDateTime()))->format('y/m/d H:i');
            }

            yield $data;
        }
    }
}
