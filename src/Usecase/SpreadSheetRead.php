<?php

namespace NORA\GoogleSdk\Usecase;

use Generator;
use Google\Service\Sheets;
use NORA\GoogleSdk\GoogleSdkInterface;
use NORA\GoogleSdk\VO\SpreadSheetId;
use NORA\GoogleSdk\VO\AccessToken;
use RuntimeException;

/**
 * SpreadSheetから値を取得する
 */
final class SpreadSheetRead
{
    public function __construct(private GoogleSdkInterface $sdk)
    {
    }

    /**
     * @psalm-return Generator
     */
    public function __invoke(SpreadSheetId $id, AccessToken $token, ?string $name = null): Generator
    {
        $service = new Sheets($this->sdk->authenticated($token));

        if ($name == null) {
            $book = $service->spreadsheets->get($id);
            $sheet = $book->getSheets()[0];
            assert($sheet instanceof \Google\Service\Sheets\Sheet);
            $name = $sheet->properties->title;
        }
        try {
            $response = $service->spreadsheets_values->get($id, $name);
        } catch (\Throwable $e) {
            throw new RuntimeException($e->getMessage());
        }

        $values = $response->getValues();

        foreach ($values as $rec) {
            foreach ($rec as $k => $v) {
                $rec[$k] = empty($v) ? null : $v;
            }
            yield $rec;
        }
    }
}
