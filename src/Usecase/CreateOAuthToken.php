<?php

declare(strict_types=1);

namespace NORA\GoogleSdk\Usecase;

use NORA\GoogleSdk\Enum\AccessType;
use NORA\GoogleSdk\Enum\Prompt;
use NORA\GoogleSdk\GoogleSdkInterface;
use NORA\GoogleSdk\VO\AccessToken;
use RuntimeException;

/**
 * OAuthToken
 */
final class CreateOAuthToken
{
    public function __construct(private GoogleSdkInterface $sdk)
    {
    }

    public function createAuthUrl(
        string $redirect_uri,
        AccessType $accessType = AccessType::OFFLINE,
        Prompt $prompt = Prompt::SELECT_ACCOUNT
    ): string
    {
        $client = $this->sdk->getClient();
        $client->setRedirectUri($redirect_uri);
        $client->setPrompt($prompt->value);
        $client->setAccessType($accessType->value);
        return $client->createAuthUrl();
    }

    public function initializeByCode(string $redirect_uri, string $code): AccessToken
    {
        $client = $this->sdk->getClient();
        $client->setRedirectUri($redirect_uri);
        $result = $client->fetchAccessTokenWithAuthCode($code);
        if (isset($result['error'])) {
            throw new RuntimeException("Token Fetch failed {$result['error']}");
        }
        $client->setAccessToken($result);
        return AccessToken::fromClient($client);
    }

    public function __invoke(int $port = 8999): AccessToken
    {
        error_reporting(E_ALL);
        set_time_limit(0);
        ob_implicit_flush();

        $redirect = "http://localhost:{$port}";
        echo $this->createAuthUrl($redirect);


        $result = $this->startStandaloneCallbackServer($port);

        $token = $this->initializeByCode(code: $result['code'], redirect_uri: $redirect);
        return $token;
    }

    /**
     * @return (mixed|string)[]
     *
     * @psalm-return array{code: string,...}
     */
    public function startStandaloneCallbackServer(int $port = 8080): array
    {
        error_reporting(E_ALL);
        set_time_limit(0);
        ob_implicit_flush();

        $server = stream_socket_server(
            "tcp://0.0.0.0:{$port}",
            $errno,
            $errmsg,
            STREAM_SERVER_LISTEN | STREAM_SERVER_BIND
        );

        assert(is_resource($server));

        $query = [];
        while ($conn = stream_socket_accept($server)) {
            echo "\r\nSocket accepted\r\n";
            while($line = fgets($conn, 1024)) {
                if ($line === "\r\n") {
                    break;
                }
                if (preg_match('/GET (.+) HTTP/', $line, $m)) {
                    $parts = parse_url($m[1]);
                    if (!isset($parts['query'])) {
                        continue;
                    }
                    parse_str($parts['query'], $query);
                    break 2;
                }
            }
            fwrite($conn, "HTTP/1.1 201 OK\r\n");
            fwrite($conn, "\r\n");
            fclose($conn);
        }
        fclose($server);

        assert(isset($query["code"]));
        assert(is_string($query["code"]));
        return $query;
    }
}
