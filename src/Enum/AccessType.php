<?php

namespace NORA\GoogleSdk\Enum;

enum AccessType : string
{
    case OFFLINE = "offline";
}
