<?php

namespace NORA\GoogleSdk\Enum;

/**
 *  {@code "none"} Do not display any authentication or consent screens. Must not be specified with other values.
 *  {@code "consent"} Prompt the user for consent.
 *  {@code "select_account"} Prompt the user to select an account.
 */
enum Prompt: string
{
    case NONE = "none";
    case CONSENT = "consent";
    case SELECT_ACCOUNT = "select_account";
}
