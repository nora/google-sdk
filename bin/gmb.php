<?php

//require_once dirname(__DIR__) . '/src/MyBusiness.php';
use Google\Service\Sheets;
use NORA\GoogleSdk\GoogleSdk;
use NORA\GoogleSdk\Usecase\CreateOAuthToken;
use NORA\GoogleSdk\VO\GoogleSdkConfig;
use NORA\Oauth\Infra\AccessTokenRepo;
use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Kvs\KvsFilesystemStorage;

require_once dirname(__DIR__) . '/vendor/autoload.php';

// Configure SDK
$sdk = new GoogleSdk(new GoogleSdkConfig(
    credentialsJson: __DIR__ . '/../tests/var/gmb.json',
    scopes: [
        'email',
        'https://www.googleapis.com/auth/business.manage'
    ]
));

$service = new CreateOAuthToken($sdk);

// Create Token Storage
$tokenRepo = new AccessTokenRepo(
    new KvsFilesystemStorage(
        option: new FilesystemStorageOption(path: __DIR__ . '/../tests/var/google-sdk')
    )
);

if (!$tokenRepo->has('gmb')) {
    // PORT 8999 to get AuthCode
    $tokenRepo->save(($service)(8999), 'gmb');
}

$token = $tokenRepo->get('gmb');
$client = $sdk->authenticated($sdk->refresh($token));
// echo "\n";
// echo "\n";
// var_dump(
//     $client->getAccessToken()['access_token']
// );
// echo "\n";
// echo "\n";
// exit();
//
if ($sdk->isAccessTokenExpired($token)) {
    echo "期限切れ\n";
    // PORT 8999 to get AuthCode
    $tokenRepo->save(($service)(8999), 'gmb');
    $token = $tokenRepo->get('gmb');
    $client = $sdk->authenticated($sdk->refresh($token));
    //exit(0);
}

// $account = new \Google\Service\MyBusinessAccountManagement($sdk->authenticated($token));
// $account = new \Google\Service\MyBusinessBusinessInformation($sdk->authenticated($token));
//
// var_Dump($account->accounts_locations->listAccountsLocations('accounts/7269683366'));

$client = $sdk->authenticated($token);
try {
    $account_service = new \Google\Service\MyBusinessAccountManagement($client);
    $account = $account_service->accounts->listAccounts()->getAccounts();
    $account_name = !empty($account[0]) ? $account[0]->getName() : null;

    var_dump($account_name);
} catch (Exception $e) {
    echo $e;
    return;
}

echo $token->getToken();
echo "\n";

echo "保存しました。";
