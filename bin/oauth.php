<?php

use Google\Service\Sheets;
use NORA\GoogleSdk\GoogleSdk;
use NORA\GoogleSdk\Usecase\CreateOAuthToken;
use NORA\GoogleSdk\VO\GoogleSdkConfig;
use NORA\Oauth\Infra\AccessTokenRepo;
use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Kvs\KvsFilesystemStorage;

require_once dirname(__DIR__) . '/vendor/autoload.php';

// Configure SDK
$sdk = new GoogleSdk(new GoogleSdkConfig(
    credentialsJson: __DIR__ . '/../tests/var/credentials.json',
    scopes: [
        'email',
        Sheets::SPREADSHEETS_READONLY
    ]
));

$service = new CreateOAuthToken($sdk);

// Create Token Storage
$token = new AccessTokenRepo(
    new KvsFilesystemStorage(
        option: new FilesystemStorageOption(path: __DIR__ . '/../tests/var/google-sdk')
    )
);

// PORT 8999 to get AuthCode
$token->save(($service)(8999), 'google');

echo "保存しました。";
