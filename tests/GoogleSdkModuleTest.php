<?php

declare(strict_types=1);

namespace NORA\GoogleSdk;

use BEAR\AppMeta\Meta;
use DateTime;
use Google\Client;
use NORA\GoogleSdk\Fake\Module\TestModule;
use NORA\GoogleSdk\Usecase\CalendarRead;
use NORA\GoogleSdk\Usecase\CreateOAuthToken;
use NORA\GoogleSdk\Usecase\SpreadSheetRead;
use NORA\GoogleSdk\VO\SpreadSheetId;
use NORA\GoogleSdk\VO\AccessToken;
use PHPUnit\Framework\TestCase;
use Ray\Di\Injector;

class GoogleSdkModuleTest extends TestCase
{
    protected Injector $injector;

    protected function setUp(): void
    {
        $this->injector = new Injector(new TestModule(
            new Meta('NORA\GoogleSdk\Fake', 'app'),
        ));
    }

    public function testGoogleClient(): void
    {
        $sdk = $this->injector->getInstance(GoogleSdkInterface::class);
        $this->assertInstanceOf(GoogleSdk::class, $sdk);

        if (!file_exists(__DIR__ . "/var/credentials.json")) {
            $this->markTestSkipped("credentials.jsonが無いのでスキップします");
        }

        if (!file_exists(__DIR__ . "/var/google-sdk/access_token")) {
            $this->markTestSkipped("tokenが無いのでスキップします");
        }

        $client = $sdk->getClient();
        $this->assertInstanceOf(Client::class, $client);

        $token = AccessToken::fromFile(__DIR__ . "/var/google-sdk/access_token");
        $token->setAccessToken($client, refresh: false);
        $this->assertTrue(is_bool($client->isAccessTokenExpired()));

        $sdk->refresh($token);

        // 更新
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
        $this->assertTrue(is_bool($client->isAccessTokenExpired()));

        $me = $client->verifyIdToken();
        assert(is_array($me));
        assert(is_string($me['email']));
        $this->assertTrue(!!filter_var($me['email'], FILTER_VALIDATE_EMAIL));
    }

    public function testSpreadSheetRead(): void
    {
        if (!file_exists(__DIR__ . "/var/google-sdk/default")) {
            $this->markTestSkipped("tokenが無いのでスキップします");
        }
        $token = AccessToken::fromFile(__DIR__ . "/var/google-sdk/default");
        assert($token instanceof AccessToken);

        $reader = $this->injector->getInstance(SpreadSheetRead::class);
        $values = ($reader)(
            id: new SpreadSheetId(
                "https://docs.google.com/spreadsheets/d/1sh8iskx00xK5Gzlwxvv9yAfJHvY7Tr-dU4CduBDYDH8/edit#gid=1939564829"
            ),
            token: $token
        );
        foreach ($values as $v) {
            $this->assertIsArray($v);
        }
        $this->assertTrue(true);
    }

    public function testCalendar(): void
    {
        if (!file_exists(__DIR__ . "/var/calendar-service.json")) {
            $this->markTestSkipped("credentials.jsonが無いのでスキップします");
        }
        $reader = $this->injector->getInstance(CalendarRead::class);
        $this->assertInstanceOf(CalendarRead::class, $reader);

        $values = ($reader)(
            calendar_id: 'hajime.work_nnebkt2oe9rq2nn6n4j4q470jk@group.calendar.google.com',
            fromDateTime: new DateTime('3 day ago'),
            toDateTime: new DateTime('now'),
        );

        foreach ($values as $v) {
            $this->assertTrue(is_array($v));
        }
    }

    public function testOAuth(): void
    {
        if (!file_exists(__DIR__ . "/var/credentials.json")) {
            $this->markTestSkipped("credentials.jsonが無いのでスキップします");
        }
        $service = $this->injector->getInstance(CreateOAuthToken::class);
        $this->assertInstanceOf(CreateOAuthToken::class, $service);

        $this->assertTrue(is_string($service->createAuthUrl('http://localhost')));
    }
}
