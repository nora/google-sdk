<?php

namespace NORA\GoogleSdk\Fake\Module;

use BEAR\AppMeta\AbstractAppMeta;
use Google\Service\Sheets;
use NORA\GoogleSdk\GoogleSdkModule;
use NORA\GoogleSdk\Usecase\CalendarRead;
use NORA\GoogleSdk\Usecase\SpreadSheetRead;
use NORA\GoogleSdk\VO\GoogleSdkConfig;
use Ray\Di\AbstractModule;

final class TestModule extends AbstractModule
{
    public function __construct(
        private AbstractAppMeta $meta,
    ) {
    }

    public function configure()
    {
        $this->install(new GoogleSdkModule(
            config: [
                'default' => new GoogleSdkConfig(
                    credentialsJson: $this->meta->appDir . '/var/credentials.json',
                    scopes: [
                        'email',
                        Sheets::SPREADSHEETS_READONLY
                    ]
                ),
                'calendar' => new GoogleSdkConfig(
                    credentialsJson: $this->meta->appDir . '/var/calendar-service.json',
                    scopes: CalendarRead::$scopes
                )
            ]
        ));

        $this->bind(SpreadSheetRead::class)->toConstructor(
            SpreadSheetRead::class,
            [
                'sdk' => 'default'
            ]
        );
        $this->bind(CalendarRead::class)->toConstructor(
            CalendarRead::class,
            [
                'sdk' => 'calendar'
            ]
        );
    }
}
